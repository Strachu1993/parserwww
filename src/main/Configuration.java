package main;

public class Configuration {

	public static String ADDRESS_URL = "http://www.info-net.com.pl/katalog/zielona-gora/informatyka-telekomunikacja/informatyka-internet--uslugi";
	
	public static String COMPANY_ID = "fir";
	public static String COMPANY_NAME = ".+\" ?>(.+)</a>";
	public static String COMPANY_ADDRESS = ".+\"adres\"><p>(.+)</p><p>(.+)</p><p>(.+)</p>";
	public static String COMPANY_MAIL = ".+\\s*.+mailto:(.+)?\" title=";
	public static String COMPANY_PHONE_NUMBER = "[.\\s\\S]*?alt=\"telefon\" ?/> *(.+)</p><div style=\"float:left;clear:both;\">";
	public static String COMPANY_WEB_SITE = "[.\\s\\S]*?href=\"http://(.+)\" title=\"Przejd�";
	public static String PHONE_REG = "(\\d{2,4} \\d{2,4} \\d{2,4} \\d{2,4}|\\d{2,4} \\d{2,4} \\d{2,4}|\\(\\d-\\d{2}\\) \\d{3} \\d{2} \\d{2} ..\\d{2})";
	public static String PAGINATOR_REG = "paginator[.\\s\\S]*?>("; 
	public static String LINK_END = "/page:";
	
}


//public static String PHONE_REG = "(\\d{2} \\d{3} \\d{2} \\d{2}|\\d{3} \\d{3} \\d{3}|\\d{3} \\d{2} \\d{2}|\\d{3} \\d{4} \\d{3}|\\(\\d-\\d{2}\\) \\d{3} \\d{2} \\d{2} ..\\d{2})";
