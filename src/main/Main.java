package main;

import java.util.List;
import dto.Company;
import parser.Parser;
import read.web.ReadWeb;

public class Main {
	
	private ReadWeb readWeb;
	private Parser parser;
	private List<Company> companies;
	private String[] links;
	private int count;
	
	public Main() {
		readWeb = new ReadWeb(Configuration.ADDRESS_URL);
		parser = new Parser(readWeb.getContentWWW());
		links = parser.getLinks();
		count = 1;
	}
	
	public void readCompanies() {
		for(int i=0 ; i<links.length ; i++) {
			System.err.println(links[i]);
			readWeb = new ReadWeb(links[i]);
			parser.setContextWWW(readWeb.getContentWWW());
			companies = parser.getCompanies(); 
		}
	} 
    	
	public void showCompanies() {
		for (Company c : companies) {
			System.out.println("\nId: " + count);
			System.out.println("Nazwa: " + c.getName());
			System.out.println("Masto: " + c.getCity().toString());
			System.out.println("Telefon: " + c.getPhoneNumber());
			System.out.println("E-mail: " + c.getMail());
			System.out.println("www: " + c.getWebPage());
			count++;
		}
	}
	
	public static void main(String[] args) {
		Main m = new Main();
		m.readCompanies();
		m.showCompanies();
	}
}
