package read.web;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.Charset;

public class ReadWeb {

	private URL url;

	public ReadWeb(String url) {
		try {
			this.url = new URL(url);
		} catch (MalformedURLException e) {
			System.err.println("Nie mo�na utworzy� po��czenia:" +url);
		}
	}

	public String getContentWWW() {
		BufferedReader download;
		String temp;
		StringBuffer source = new StringBuffer("");
		
		try {
			download = new BufferedReader(new InputStreamReader(url.openStream(), Charset.forName("UTF-8")));
			
			while ((temp = download.readLine()) != null) 
				source.append((temp + "\n")); 
			
			download.close();
		} catch (IOException e) {
			System.err.println("B��d podczas pobierania zawarto�ci strony:" + url.toString());
		}
		
		return source.toString();
	}

}
