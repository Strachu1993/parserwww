package parser;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import dto.City;
import dto.Company;

import static main.Configuration.*;

public class Parser {

	private Pattern pattern;
	private Matcher matcher;
	private String contextWWW;
	private List<Company> companies;

	public Parser(String contextWWW) {
		this.contextWWW = contextWWW;
		companies = new ArrayList<Company>();
	}

	public void setContextWWW(String contextWWW) {
		this.contextWWW = contextWWW;
	}

	public int getIndexByRex(String contextWWW, String regexp) {
		pattern = Pattern.compile(regexp);
		matcher = pattern.matcher(contextWWW);

		return matcher.find() ? matcher.start() : -1;
	}
		
	public List<Company> getCompanies() {

		int i = 0, err = 0;
		while (err != 5) {
			String id = COMPANY_ID + i;
			matcher = doRegExp(id);

			if (matcher.find()) {
				String name = id + COMPANY_NAME;      
				String address = COMPANY_ADDRESS;   
				String mail = id + COMPANY_MAIL;    
				String phoneNumber = id + COMPANY_PHONE_NUMBER;   
				String webSite = id + COMPANY_WEB_SITE;  

				matcher = doRegExp(name + address);
				Company c = new Company();

				if (isExistGroup(4)) {
					c.setName(matcher.group(1));
					c.setCity(new City(matcher.group(3), matcher.group(4), matcher.group(2)));
				}

				matcher = doRegExp(mail);
				if (isExistGroup(1))
					c.setMail(matcher.group(1));

				matcher = doRegExp(phoneNumber);
				if (isExistGroup(1))
					c.setPhoneNumber(getManyMatching(matcher.group(1)));

				matcher = doRegExp(webSite);
				if (isExistGroup(1))
					c.setWebPage(matcher.group(1));

				companies.add(c);				
			} else {
				err++;
			}
			i++;
		}
		return companies;
	}
	
	public String[] getLinks() {
		int[] paginators = getPaginator();
		List<String> result = new ArrayList<String>();

		for(int i=0 ; i<paginators.length ;i++) 			
			result.add(ADDRESS_URL+LINK_END+paginators[i]);
						
		return result.stream().toArray(String[]::new);
	}
	
	private int[] getPaginator() { 
		String paginatorReg;
		List<Integer> result = new ArrayList<Integer>();
		
		int count = 1;
		do {
			paginatorReg = PAGINATOR_REG+count+")<";
			matcher = doRegExp(paginatorReg); 
			if (isExistGroup(1)) {
				result.add(Integer.parseInt(matcher.group(1)));
			}else
				break;
			count++;
		} while (true);

		return result.stream().mapToInt(i -> i).toArray();
	}
	
	private String getManyMatching(String text) {
		String phoneReg = (PHONE_REG);
		String result = "";

		do {
			matcher = doRegExp(text, phoneReg); 
			if (isExistGroup(1)) {
				String help = matcher.group(1);
				result += help + ", ";
				text = text.substring(matcher.start() + help.length());
			} else
				break;
		} while (true);

		return result.substring(0, result.length() - 2);
	}

	private boolean isExistGroup(int count) {
		return matcher.find() && matcher.groupCount() == count;
	}

	private Matcher doRegExp(String reg) {
		pattern = Pattern.compile(reg);
		return pattern.matcher(contextWWW);
	}

	private Matcher doRegExp(String text, String reg) {
		pattern = Pattern.compile(reg);
		return pattern.matcher(text);
	}

}
