package dto;

public class City {

	private String name = "---";
	private String region = "---";
	private String streat = "---";

	public City(String name, String region, String streat) {
		super();
		this.name = name;
		this.region = region;
		this.streat = streat;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getStreat() {
		return streat;
	}

	public void setStreat(String streat) {
		this.streat = streat;
	}

	@Override
	public String toString() {
		return "City [name=" + name + ", region=" + region + ", streat=" + streat + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((region == null) ? 0 : region.hashCode());
		result = prime * result + ((streat == null) ? 0 : streat.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		City other = (City) obj;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (region == null) {
			if (other.region != null)
				return false;
		} else if (!region.equals(other.region))
			return false;
		if (streat == null) {
			if (other.streat != null)
				return false;
		} else if (!streat.equals(other.streat))
			return false;
		return true;
	}

}
